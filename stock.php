<?php include 'includes/connect.php';
error_reporting(0);
$succ = $_GET['success'];
?>

<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="semantic/semantic.min.css">
    <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap JS -->
    <script src="semantic/semantic.min.js"></script>

    <title>Sawaitii Butchery</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $succ; ?>";
    if (val==1){
      alert("Order sent successfull, please hold as we get back to you!");
    }
    </script>

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
      <!-- header -->
      <div class="top-nav center">
            <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
          </div>
        <!-- menu start -->
        <div class="ui secondary pointing menu">
           <a href="http://localhost/butchery/admin_panel.php" class="item">
            Meat Orders
          </a>
          <a href="http://localhost/butchery/users.php" class=" item">
            Users
          </a>
          <a href="http://localhost/butchery/stock.php" class="active item">
            View Stocks
          </a>
          <a href="active_orders.php" class="item">
            Active Orders
          </a>
          <div class="right menu">
            <a href="http://localhost/butchery/" class="ui item">
              Logout
            </a>
          </div>
        </div>
  <!-- menu end -->
  
      <div class="ui segment">
      
      <table class="ui compact celled definition table">
          <thead class="full-width">
            <?php 
            $sql = "SELECT * FROM stocks";
            $result = $conn->query($sql);

             if ($result->num_rows > 0) {
            echo'
            <tr>
              <th></th>
              <th>Stock ID</th>
              <th>Stock Name</th>
              <th>Stock Description</th>
              <th>Unit Cost</th>
              <th>Quantity</th>
              <th>Created Date</th>
            </tr>';}
          ?>
          </thead>
          <tbody>
          <?php
            
            while($row = $result->fetch_assoc()) {
              echo '
              <tr>
              <td class="collapsing">
                  <div class="ui fitted slider checkbox">
                    <input type="checkbox"> <label></label>
                  </div>
              </td>
              <td>'.$row["stock_id"].'</td><td>'.$row["stock_name"].'</td><td>'.$row["description"].'</td><td>'.$row["stock_unit_cost"].'</td><td>'.$row["stock_quantity"].'</td><td>'.$row["stock_date_created"].'</td>
              </tr>';
            }
               ?>
          </tbody>
          <tfoot class="full-width">
            <tr>
              <th></th>
              <th colspan="4">
                <div class="ui right floated small primary labeled icon button">
                  <i class="user icon"></i> <a href="add_stock.php" style="text-decoration: none; color: #ffffff;"> Add Stock</a>
                </div>  
                <div class="ui small  button">
                  Approve
                </div>
                <div class="ui small  disabled button">
                  Approve All
                </div>
              </th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div> 
  </div>
</body>
</html>