-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2018 at 10:42 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `butchery`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `cust_id` int(11) NOT NULL,
  `cust_fname` varchar(100) NOT NULL,
  `cust_sname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `address` text NOT NULL,
  `phone` int(11) NOT NULL,
  `title` text NOT NULL,
  `details` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_cost` decimal(10,0) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `code` varchar(4) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `first_name`, `last_name`, `address`, `phone`, `title`, `details`, `quantity`, `unit_cost`, `date`, `code`, `total`, `active`) VALUES
(1, 'john', 'Doe', 'john@example.com', 72287877, 'Doe', 'Doe', 5, '41', '2018-06-27 02:18:59', '', '0', 0),
(2, 'Cossy', 'Samdoh', '133 Mogotio Village', 717084613, 'Chicken', 'White Chicken Meat', 2, '400', '2018-06-27 02:42:19', 'werd', '800', 0),
(3, 'Cossy', 'Samdoh', '133 Mogotio Village', 717084613, 'Chicken', 'White Chicken Meat', 2, '400', '2018-06-27 02:45:33', 'werd', '800', 2),
(4, 'Ciustomer2', 'Majani', 'Majani', 89988999, 'Goat Meat', 'Goat Meat', 34, '150', '2018-06-27 02:46:17', 'sdfr', '5100', 0),
(5, 'Simon', 'Wangila', 'Lomolo', 87887478, 'Chicken', 'White Chicken Meat', 3, '400', '2018-06-27 02:48:11', 'hjyh', '1200', 1),
(6, 'Cossy', 'Samdoh', '133 Mogotio Village', 717084613, 'Goat Meat', 'Goat Meat', 34, '150', '2018-06-27 02:59:19', 'werd', '5100', 1),
(7, 'Cossy Name', 'Wangila', '133 Mogotio Village', 717084613, 'Goat Meat', 'Goat Meat', 2, '150', '2018-07-04 10:48:25', 'werd', '300', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_serial` varchar(5) NOT NULL,
  `payment_amount` decimal(10,0) NOT NULL,
  `payment_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `message`, `date`, `created_by`) VALUES
(1, 'Hello?', '0000-00-00 00:00:00', ''),
(2, 'Hello', '0000-00-00 00:00:00', ''),
(3, 'Hello CodeChallengeSwahiliPot\r\nA Code Challenge for the SwahiliPot developer community based off USSD technology', '0000-00-00 00:00:00', ''),
(4, '', '2018-07-21 17:02:43', ''),
(5, 'Hello there? testing still', '2018-07-21 17:02:59', '');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `sales_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `payment_mode` varchar(15) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `stock_id` int(11) NOT NULL,
  `cat_id` varchar(30) NOT NULL,
  `stock_name` varchar(100) NOT NULL,
  `description` varchar(300) NOT NULL,
  `stock_unit_cost` decimal(10,0) NOT NULL,
  `stock_units` varchar(15) NOT NULL,
  `stock_date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stock_quantity` int(11) NOT NULL,
  `image` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`stock_id`, `cat_id`, `stock_name`, `description`, `stock_unit_cost`, `stock_units`, `stock_date_created`, `stock_quantity`, `image`) VALUES
(3, '2', 'Beef', 'Cow Meat', '120', 'Kgs', '2018-06-17 17:23:52', 100, 'uploads/Beef.jpg'),
(4, '1', 'Chicken', 'White Chicken Meat', '400', 'Kgs', '2018-06-17 17:24:54', 20, 'uploads/Chicken.jpg'),
(5, '4', 'Goat Meat', 'Goat Meat', '150', 'Kgs', '2018-06-17 17:25:47', 60, 'uploads/Goat Meat.jpg'),
(6, '1', 'Choped Pork', 'Pig meat chopped', '200', 'Kgs', '2018-07-04 09:46:26', 200, 'uploads/PorkChop.jpg'),
(7, '1', 'Camel', 'Camel Meat', '200', 'Kgs', '2018-07-17 19:48:54', 200, 'uploads/Camel.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `stock_categories`
--

CREATE TABLE `stock_categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_desc` text NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_categories`
--

INSERT INTO `stock_categories` (`cat_id`, `cat_name`, `cat_desc`, `date_created`) VALUES
(1, 'Chicken', 'Poultry Chicken Meat', '2018-06-17 12:02:37'),
(2, 'Beef', 'Cow Meat', '2018-06-17 12:03:09'),
(3, 'Pock', 'Pig Meat', '2018-06-17 12:03:29'),
(4, 'Goat', 'Goat Meat', '2018-06-17 12:03:55');

-- --------------------------------------------------------

--
-- Table structure for table `userbkp`
--

CREATE TABLE `userbkp` (
  `id` int(11) NOT NULL,
  `first_name` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userbkp`
--

INSERT INTO `userbkp` (`id`, `first_name`, `last_name`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, '0', '', 'sam', '8F9DiRYuYNM27A9U8K1jH6veleYHx9cZ', 'password123', NULL, 'samdohsol@gmail.com', 10, '0000-00-00 00:00:00', 1529134456),
(2, '0', '', 'admin', '5PFfhgXtKiye_Wg97mv9NisF2iTj2Wz9', 'admin', NULL, 'user@admin.com', 10, '0000-00-00 00:00:00', 1529142280),
(3, '0', '', 'Marry Akinyi', 'q8J3z3da1b_Hu5-KUW3YZW8Z51Xq0u3X', 'mary', NULL, 'marry@yahoo.com', 10, '0000-00-00 00:00:00', 1529276118);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `fname` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `pin` varchar(4) NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`fname`, `username`, `pin`, `password`, `email`, `created_date`) VALUES
('Caroline', 'admin', '1111', 'admin', 'admin@gmail.com', '2018-07-25 13:32:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`sales_id`),
  ADD KEY `stock_id` (`stock_id`),
  ADD KEY `cust_id` (`cust_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `payment_id` (`payment_id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`stock_id`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `cat_id_2` (`cat_id`),
  ADD KEY `cat_id_3` (`cat_id`),
  ADD KEY `cat_id_4` (`cat_id`);

--
-- Indexes for table `stock_categories`
--
ALTER TABLE `stock_categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `userbkp`
--
ALTER TABLE `userbkp`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `cust_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `sales_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stock_categories`
--
ALTER TABLE `stock_categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `userbkp`
--
ALTER TABLE `userbkp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`stock_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`cust_id`) REFERENCES `customers` (`cust_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `userbkp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_ibfk_4` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`payment_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
