<?php include 'includes/connect.php';
error_reporting(0);
$success = $_GET['success'];
$succ = $_GET['success_p'];
$suc = $_GET['success_d'];
?>

<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="semantic/semantic.min.css">
    <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap JS -->
    <script src="semantic/semantic.min.js"></script>

    <title>Sawaitii Butchery</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $success; ?>";
    if (val==1){
      alert("Order sent successfull, please hold as we get back to you!");
    }
    var val = "<?php echo $succ; ?>";
    if (val==1){
      alert("Order approved successfull!");
    }
    var val = "<?php echo $suc; ?>";
    if (val==1){
      alert("Order Closed successfull!");
    }
    </script>

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
      <!-- header -->
      <div class="top-nav center">
            <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
          </div>
        <!-- menu start -->
        <div class="ui secondary pointing menu">
           <a href="http://localhost/butchery/admin_panel.php" class="item">
            Meat Orders
          </a>
          <a href="http://localhost/butchery/users.php" class=" item">
            Users
          </a>
          <a href="http://localhost/butchery/stock.php" class="item">
            View Stocks
          </a>
          <a href="active_orders.php" class=" active item">
            Active Orders
          </a>
          <div class="right menu">
            <a href="http://localhost/butchery/" class="ui item">
              Logout
            </a>
          </div>
        </div>
  <!-- menu end -->
  
      <div class="ui segment">
      
      <table class="ui compact celled definition table">
          <thead class="full-width">
            <?php 
            $sql = "SELECT * FROM orders WHERE active=1 ORDER BY order_id DESC";
            $result = $conn->query($sql);

             if ($result->num_rows > 0) {
            echo'
            <tr>
              <th>Stock Serial</th>
              <th>First Name</th>
              <th>Second Name</th>
              <th>Email Address</th>
              <th>Phone</th>
              <th>Stock Title</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Unit Cost</th>
              <th>Date</th>
              <th>Code</th>
              <th>Total</th>
              <th>Action</th>
            </tr>';}
          ?>
          </thead>
          <tbody>
          <?php
            
            while($row = $result->fetch_assoc()) {
              echo '
              <tr>
              
              <td>'.$row["order_id"].'</td>
              <td>'.$row["first_name"].'</td>
              <td>'.$row["last_name"].'</td>
              <td>'.$row["address"].'</td>
              <td>'.$row["phone"].'</td>
              <td>'.$row["title"].'</td>
              <td>'.$row["details"].'</td>
              <td>'.$row["quantity"].'</td>
              <td>'.$row["unit_cost"].'</td>
              <td>'.$row["date"].'</td>
              <td>'.$row["code"].'</td>
              <td>'.$row["total"].'</td>
              <td><a href="delete_order.php?id='.$row["order_id"].'">Close Order</td>
              </tr>';
            }
               ?>
          </tbody>
          <tfoot class="full-width">
            
          </tfoot>
        </table>
      </div>
    </div> 
  </div>
</body>
</html>