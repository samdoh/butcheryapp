<?php include 'includes/connect.php';
error_reporting(0);
$succ = $_GET['success'];
?>

<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="semantic/semantic.min.css">
    <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap JS -->
    <script src="semantic/semantic.min.js"></script>

    <title>Sawaitii Butchery</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $succ; ?>";
    if (val==1){
      alert("Order sent successfull, please hold as we get back to you!");
    }
    </script>

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
      <!-- header -->
      <div class="top-nav center">
            <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
          </div>
        <!-- menu start -->
        <div class="ui secondary pointing menu">
           <a href="http://localhost/butchery/admin_panel.php" class="item">
            Meat Orders
          </a>
          <a href="http://localhost/butchery/users.php" class="active item">
            Users
          </a>
          <a href="http://localhost/butchery/stock.php" class="item">
            View Stocks
          </a>
          <a href="active_orders.php" class="item">
            Active Orders
          </a>
          <div class="right menu">
            <a href="http://localhost/butchery/" class="ui item">
              Logout
            </a>
          </div>
        </div>
  <!-- menu end -->
  
      <div class="ui segment">
      
      <table class="ui compact celled definition table">
          <thead class="full-width">
            <?php 
            $sql = "SELECT * FROM users";
            $result = $conn->query($sql);

             if ($result->num_rows > 0) {
            echo'
            <tr>
              <th>User ID</th>
              <th>User PIN</th>
              <th>Full Name</th>
              <th>User Name</th>
              <th>Email</th>
              <th>Created Date</th>
            </tr>';}
          ?>
          </thead>
          <tbody>
          <?php
            
            while($row = $result->fetch_assoc()) {
              echo '
              <tr>
              <td>'.$row["id"].'</td>
              <td>'.$row["pin"].'</td>
              <td>'.$row["fname"].'</td>
              <td>'.$row["username"].'</td>
              <td>'.$row["email"].'</td>
              <td>'.$row["created_date"].'</td>
              </tr>';
            }
               ?>
          </tbody>
          <tfoot class="full-width">
            <tr>
              <th>
                <div class="ui right floated small primary labeled icon button">
                 <a href="signup.php" style="text-decoration: none; color: #ffff;"><i class="user icon"></i> Add User</a> 
                </div>  
               
              </th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div> 
  </div>
</body>
</html>