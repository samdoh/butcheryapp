<?php include 'includes/connect.php';?>

<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="semantic/semantic.min.css">
    <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap JS -->
    <script src="semantic/semantic.min.js"></script>

    <title>Sawaitii Butchery</title>

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
      <!-- header -->
      <div class="top-nav center">
            <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
          </div>
        <!-- menu start -->
        <div class="ui secondary pointing menu">
          
          <a href="http://localhost/butchery/" class=" item">
            Sawaitii Butchery
          </a>
          <a href="http://localhost/butchery/store.php" class="active item">
            Meat Store
          </a>
          <a  href="blog.php" class="item">
            Blog
          </a>
          <div class="right menu">
            <a class="ui item">
              Login
            </a>
          </div>
        </div>
  <!-- menu end -->
  
      <div class="ui segment">
        <div class="ui cards">
        <?php
          $sql = "SELECT * FROM stocks";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
              // echo "<table><tr><th>ID</th><th>Name</th></tr>";
              // output data of each row
              while($row = $result->fetch_assoc()) {
              echo '<div class="card">
                    <div class="image">
                      <img src="'.$row["image"].'">
                    </div>
                    <div class="content">
                      <div class="header">'.$row["stock_id"].' : '.$row["stock_name"].'</div>
                      <div class="meta">
                        <a>Friends</a>
                      </div>
                      <div class="description">
                        Description : '.$row["description"].'
                      </div>
                    </div>
                    <div class="extra content">
                      <span class="right floated">
                        Since : '.$row["stock_date_created"].'
                      </span>
                      <span>
                        '.$row["stock_quantity"].' '.$row["stock_units"].' Remaining  
                      </span>
                      <a href="/order.php?id='.$row["stock_id"].'"><i class="add icon"></i>Order Product >></a>
                    
                    </div>
                    <div class="ui bottom attached button">
                    <a href="/order.php?id='.$row["stock_id"].'"><i class="add icon"></i>Order Product >></a>
                    </div>
                  </div>';
              }
          } else {
              echo "0 results";
          }
          $conn->close();
    ?>
       
      <!-- <div class="container">
        <div class="ui pointing menu">
          <a class="active item">
            Home
          </a>
          <a href="store.html" class="item">
            Store
          </a>
          <a class="item">
            Order
          </a>
          <div class="right menu">
            <div class="item">
              <div class="ui transparent icon input">
                <input type="text" placeholder="Search...">
                <i class="search link icon"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="ui segment">
         <h1>Welcome to Sawaitii Butchery</h1> <p>
            Hello
          </p>
        </div>
         -->
         <!-- menu end -->
       
      </div>
        <p></p>
      </div>
        

      </div>
    </div>
    <!-- Modal start -->
      <div class="ui modal order">
        <i class="close icon"></i>
        <div class="header">
          Profile Picture
        </div>
        <div class="image content">
          <div class="ui medium image">
            <img src="https://semantic-ui.com/images/avatar2/large/rachel.png">
          </div>
          <div class="description">
            <div class="ui header">We've auto-chosen a profile image for you.</div>
            <p>We've grabbed the following image from the <a href="https://www.gravatar.com" target="_blank">gravatar</a> image associated with your registered e-mail address.</p>
            <p>Is it okay to use this photo?</p>
          </div>
        </div>
        <div class="actions">
          <div class="ui black deny button">
            Nope
          </div>
          <div class="ui positive right labeled icon button">
            Yep, that's me
            <i class="checkmark icon"></i>
          </div>
        </div>
      </div>
      <!-- Modal End -->

   
  </body>

  <script type="text/javascript">
    var current page = "{{ your_var }}";
    $(function(){
      $("#order").click(function(){
        $(".order").modal('show');
      });
      $(".order").modal({
        closable: true
      });
    });
  </script>
</html>