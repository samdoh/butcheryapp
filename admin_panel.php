<?php include 'includes/connect.php';
error_reporting(0);
$succ = $_GET['success'];
?>

<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="semantic/semantic.min.css">
    <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap JS -->
    <script src="semantic/semantic.min.js"></script>

    <title>Sawaitii Butchery</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $succ; ?>";
    if (val==1){
      alert("Order sent successfull, please hold as we get back to you!");
    }
    </script>

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
      <!-- header -->
      <div class="top-nav center">
            <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
          </div>
        <!-- menu start -->
        <div class="ui secondary pointing menu">
          
         
          <a href="http://localhost/butchery/admin_panel.php" class="active item">
            Meat Orders
          </a>
          <a href="http://localhost/butchery/users.php" class=" item">
            Users
          </a>
          <a href="http://localhost/butchery/stock.php" class="item">
            View Stocks
          </a>
          <a href="active_orders.php" class="item">
            Active Orders
          </a>
          <div class="right menu">
            <a href="http://localhost/butchery/" class="ui item">
              Logout
            </a>
          </div>
        </div>
  <!-- menu end -->
  
      <div class="ui segment">

        <div class="ui cards">
        <?php
          $sql = "SELECT * FROM orders WHERE active=0 ORDER BY date DESC";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
              // echo "<table><tr><th>ID</th><th>Name</th></tr>";
              // output data of each row
              while($row = $result->fetch_assoc()) {
              echo '<div class="card">
                    <div class="content">
                      <div class="header">Customer: '.$row["first_name"].' - '.$row["last_name"].'</div>
                      <hr>
                      <div class="meta">
                       Description : '.$row["details"].'
                      </div>
                      <div class="description">
                        Address : '.$row["address"].'
                      </div><hr>
                      <div class="description">
                        MPESA Code: '.$row["code"].'  |  Ksch : '.$row["total"].';
                      </div>
                    </div>
                    <div class="extra content">
                      <span class="right floated">
                        Date : '.$row["date"].'
                      </span>
                      <span>
                        Amount : '.$row["quantity"].' '.$row["stock_units"].'  
                      </span>
                    </div>
                    <div class="ui bottom attached button">
                    <button class="ui button yellow create_btn" type="button" id="order">
                      <a href="approve_order.php?id='.$row["order_id"].'"><i class="add icon"></i>Approve Order >></a>
                    </button>
                    </div>
                  </div>';
              }
          } else {
              echo "0 results";
          }
          $conn->close();
    ?>  
      </div>
      
    </div> 

      </div>
    </div>
    <!-- Modal start -->
      <div class="ui modal order">
        <i class="close icon"></i>
        <div class="header">
          Profile Picture
        </div>
        <div class="image content">
          <div class="ui medium image">
            <img src="https://semantic-ui.com/images/avatar2/large/rachel.png">
          </div>
          <div class="description">
            <div class="ui header">We've auto-chosen a profile image for you.</div>
            <p>We've grabbed the following image from the <a href="https://www.gravatar.com" target="_blank">gravatar</a> image associated with your registered e-mail address.</p>
            <p>Is it okay to use this photo?</p>
          </div>
        </div>
        <div class="actions">
          <div class="ui black deny button">
            Nope
          </div>
          <div class="ui positive right labeled icon button">
            Yep, that's me
            <i class="checkmark icon"></i>
          </div>
        </div>
      </div>
      <!-- Modal End -->

   
  </body>

  <script type="text/javascript">
    var current page = "{{ your_var }}";
    $(function(){
      $("#order").click(function(){
        $(".order").modal('show');
      });
      $(".order").modal({
        closable: true
      });
    });
  </script>
</html>