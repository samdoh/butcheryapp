<?php
include 'includes/connect.php';

error_reporting(0);
$succ = $_GET['success'];
?>

<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/semantic.min.css">
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap JS -->
    <script src="js/semantic.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <title>Sawaitii Butchery</title>


  <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $succ; ?>";
    if (val==2){
      alert("Order not sent successfull, please select less stock quantity!");
    }
    </script>

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
        <!-- header -->
        <div class="top-nav center">
              <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
            </div>
          <!-- menu start -->
          <div class="ui secondary pointing menu">
             <a href="http://localhost/butchery/store.php" class="item">
                Back to Meat Store |
              </a>
            <a class="active item">
              Meat Order >>
            </a>
            <div class="right menu">
              <a href="login.php" class="ui item">
                Login
              </a>
            </div>
          </div>
    <!-- menu end -->

      <div class="ui segment">
        <?php
          $prod_id = $_GET["id"];
          $sql = "SELECT * FROM stocks WHERE stock_id=".$prod_id."";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
              // echo "<table><tr><th>ID</th><th>Name</th></tr>";
              // output data of each row
              while($row = $result->fetch_assoc()) {
              echo '
                    <form class="ui form" method="post" action="includes/add_order.php">
                      <h4 class="ui dividing header">Shipping Information</h4>
                      <div class="field">
                        <label>Name</label>
                        <div class="two fields">
                          <div class="field">
                            <input type="text" name="fname" placeholder="First Name *" required>
                          </div>
                          <div class="field">
                            <input type="text" name="lname" placeholder="Last Name">
                          </div>
                        </div>
                      </div>
                      <div class="field">
                        <label>Billing Address</label>
                        <div class="fields">
                          <div class="twelve wide field">
                            <input type="text" name="address" placeholder="Street Address*" required>
                          </div>
                          <div class="four wide field">
                            <input type="text" name="phone" placeholder="Phone*" required>
                          </div>
                        </div>
                      </div>
                      <div class="two fields">
                      </div>
                      <h4 class="ui dividing header">Product Information</h4>
                      
                       <div class="six wide field">
                          <div class="field">
                          <input type="text" name="stock_id" value="'.$row["stock_id"].'" style="background-color:orange;" disabled>

                            <label>Name:</label> <input type="text" name="stock" maxlength="4" value="'.$row["stock_name"].'" style="background-color:orange;" disabled>
                          </div>
                          <div class="field">
                            <label>Description:</label> <input type="text" style="background-color:orange;" name="desc" maxlength="4" value="'.$row["description"].'" disabled>
                          </div>
                          <div class="field">
                            <label>Price:</label> <input type="text" style="background-color:orange;" name="cost" maxlength="16" value="'.$row["stock_unit_cost"].'" disabled>
                          </div>
                          <div class="field">
                          <label>Quantity *</label>
                          <input type="text" name="quantity_available" value="'.$row["stock_quantity"].'" disabled style="background-color:orange;">
                          <input type="text" name="quantity"   maxlength="16" placeholder="e.g 5" required>
                          </div>
                        <div class="six wide field">
                          <img src="images/mpesa.bmp" width="" height="200">
                        </div>
                        </div>
                        
                        
                        <div class=" field">
                          <label>Last 4 digit M-Pesa Code</label>
                          <div class="two fields">
                            <div class="field">
                              <input type="text" name="code" maxlength="4" placeholder="Code" required>
                            </div>
                          </div>
                        </div>
                      <input class="ui button" type="submit" value="Submit Order">
                    </form>';
              }
          } else {
              echo "0 results";
          }
          $conn->close();
    ?>
      </div>
       
      </div>

      </div>
    </div>
    
   
  </body>
  <footer class="center">
     <?php include 'includes/footer.php';?>
  </footer>
</html>