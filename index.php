
<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/semantic.min.css">
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Bootstrap JS -->
    <script src="js/semantic.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>

    <title>Sawaitii Butchery</title>

    <link rel="shortcut icon" href="fav.png">

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
        <!-- header -->
        <div class="top-nav center">
              <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
            </div>
          <!-- menu start -->
          <div class="ui secondary pointing menu">
            
            <a href="http://localhost/butchery/" class="active item">
              Sawaitii Butchery
            </a>
            <a href="http://localhost/butchery/store.php" class="item">
              Meat Store
            </a>
            <a  href="blog.php" class="item">
              Blog
            </a>
            <div class="right menu">
              <a href="login.php" class="ui item">
                Login
              </a>
            </div>
          </div>
    <!-- menu end -->

      <div class="ui segment">
        <p></p>
         <div class="ui styled fluid accordion">
          <div class="active title">
            <i class="dropdown icon"></i>
            Abstract?
          </div>
          <div class="active content">
            <p>Online butchery management system is a web based system that will be developed to manage services production at sawaitii butchery that is located in Baringo County. Problems faced by the current system include loss of vital customers’ information, slow serving of the customers, immediate running out of the stocks and severe loss of money. The system developed will help to manage the butchery to keep all tracks of sales record of each customers, display category of meat present, online booking of meat and online payment.</p><p> The main objective of this research proposal is to develop a system that will improve butchery production so as to manage immediate running out of stocks. System modules include sales tracking record module, advertisement module, online booking module and online payment module. The importance of this proposal is to make services production to the butchery fast. The research design tools include dataflow diagrams, flow chart and physical ones such as user graphic interface. </p><p>Programming language will include PHP, HTML, JavaScript and the database will be designed using and structured query languages (MySQL). The methods of data collections includes observations, questionnaire, interview and document review. Economical, technical, operational and social will be used to test how feasible the system will be developed; it will solve most of the problems faced by the case study</p>
          </div>
          <div class="title">
            <i class="dropdown icon"></i>
            Background of the Study?
          </div>
          <div class="content">
            <p>There are many breeds of dogs. Each breed varies in size and temperament. Owners often select a breed of dog that they find to be compatible with their own lifestyle and desires from a companion.</p>
          </div>
          <div class="title">
            <i class="dropdown icon"></i>
            How do you acquire a dog?
          </div>
          <div class="content">
            <p>Three common ways for a prospective owner to acquire a dog is from pet shops, private owners, or shelters.</p>
            <p>A pet shop may be the most convenient way to buy a dog. Buying a dog from a private owner allows you to assess the pedigree and upbringing of your dog before choosing to take it home. Lastly, finding your dog from a shelter, helps give a good home to a dog who may not find one so readily.</p>
          </div>
        </div>
      </div>
       
      </div>

      </div>
    </div>
    
   
  </body>
  <footer class="center">
     <?php include 'includes/footer.php';?>
  </footer>
</html>