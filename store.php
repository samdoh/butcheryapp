<?php 
// Include the connection file
include 'includes/connect.php';

// for success message
error_reporting(0);
$succ = $_GET['success'];
?>

<!doctype html>
<html lang="en">
  <head>

    <link rel="shortcut icon" href="fav.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="semantic/semantic.min.css">
    <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
     <link rel="stylesheet" href="css/semantic.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- semantic JSs -->
    <script src="semantic/semantic.min.js"></script>

    <title>Sawaitii Butchery</title>

    <!-- Popup -->
    <script type="text/javascript">
    var val = "<?php echo $succ; ?>";
    if (val==1){
      alert("Order sent successfull, please hold as we get back to you!");
    }
    </script>

  </head>
  <body>
    <div class="container-fluid" id="">
      <div class="container" >
      <!-- header -->
      <div class="top-nav center">
            <h3>ONLINE BUTCHERY MANAGEMENT SYSTEM</h3>
          </div>
        <!-- menu start -->
        <div class="ui secondary pointing menu">
          
          <a href="http://localhost/butchery/" class=" item">
            Sawaitii Butchery
          </a>
          <a href="http://localhost/butchery/store.php" class="active item">
            Meat Store
          </a>
          <a  href="blog.php" class="item">
            Blog
          </a>
          <div class="right menu">
            <a href="login.php" class="ui item">
              Login
            </a>
          </div>
        </div>
  <!-- menu end -->
  <!-- scroll effect start -->
 
      <div class="ui segment">
        <marquee><p>Welcome to our store, please select a product to order now!!!</p></marquee>
        <div class="ui cards" >
        <?php
          $sql = "SELECT * FROM stocks ORDER BY stock_name ASC";
          $result = $conn->query($sql);

          if ($result->num_rows > 0) 
          {

              while($row = $result->fetch_assoc()) {
              echo '<div class="card" >
                    <div class="image">
                      <img src="'.$row["image"].'">
                    </div>
                    <div class="content">
                      <div class="header">'.$row["stock_id"].' : '.$row["stock_name"].'</div>
                      <div class="meta">
                        <a>Active</a>
                      </div>
                      <div class="description">
                        Description : '.$row["description"].'
                      </div>
                    </div>
                    <div class="extra content">
                      <span class="right floated">
                        Since : '.$row["stock_date_created"].'
                      </span>
                      <span>
                        '.$row["stock_quantity"].' '.$row["stock_units"].' Remaining  
                      </span>
                    </div>
                    <div class="ui bottom attached button">
                    <button class="ui button yellow create_btn" type="button" id="order">
                      <a href="http://localhost/butchery/order.php?id='.$row["stock_id"].'"><i class="add icon"></i>Order Product >></a>
                    </button>
                    </div>
                  </div>';
              }
          } else {
              echo "Ooohhps, No record found!!!";
          }
          $conn->close();
    ?>
       
      </div>
        <p></p>
    </div> 

      </div>
    </div>
    
  </body>
</html>